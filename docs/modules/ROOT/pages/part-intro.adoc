Dans cette première partie de notre support Formatux, nous vous proposons de découvrir, ou redécouvrir **les notions essentielles de l'administration Linux**.

Dans un premier temps, nous discuterons de **Linux**, des distributions, et de tout l'écosystème autour de notre système d'exploitation.

Nous verrons ensuite, les **commandes utilisateurs** indispensables à la prise en main de Linux. Les utilisateurs plus aguérris pourront également consulter le chapitre consacré aux commandes un peu plus "avancées".

Un chapitre qui nous semble toujours aussi important vient ensuite : la **prise en main de l'éditeur `VI`**, ou tout au moins sa démystification : savoir ouvrir un ficher, enregistrer, quitter ou quitter sans enregistrer. Pour les autres fonctionnalités, l'utilisateur sera plus à l'aise avec elles au fur et à mesure de son utilisation de cet éditeur pas comme les autres mais tellement puissant. Un mémo des commandes `VI` est disponible dans notre partie Annexes du support global.

Nous pourrons ensuite entrer dans le vif du sujet et dans le fonctionnement de Linux pour découvrir comment le système gère :

* les **utilisateurs**,
* les **systèmes de fichiers**,
* les **processus**.

Nous ferons un aparté sur les **sauvegardes**, sujet toujours si important qu'il est indispensable d'en parler : nous ne ferons jamais assez de sauvegardes. Nous découvrirons deux outils : `tar` mais également `cpio`, moins répandu, qui dans certains cas peut être très intéressant.

Nous reviendrons à la gestion du système avec le **démarrage**, qui a énormément évolué ces dernières années depuis l'arrivée de `systemd`. Le chapitre __Démarrage du système (init)__ traitera des versions legacy toujours bien présentes dans les parcs informatiques (debian 7, RHEL 6) tandis que le chapitre __Démarrage du système sous CentOS 7 (systemd)__ traitera des versions plus modernes (debian 8, RHEL 7) et permettra à l'administrateur système de se mettre à niveau sur ce sujet.

Nous concluerons cette partie en étudiant la **gestion des tâches**, la **mise en oeuvre du réseau** et l' **installation des logiciels**.

Bonne lecture.
